UPDATE public.customer
SET
  first_name = 'Maruf',
  last_name = 'Mamatqulov',
  email = 'maruf.mamatqulov@example.com',
  address_id = (SELECT address_id FROM public.address ORDER BY random() LIMIT 1)
WHERE
  customer_id IN (
    SELECT customer_id
    FROM public.customer
    WHERE
      customer_id IN (
        SELECT customer_id
        FROM public.payment
        GROUP BY customer_id
        HAVING COUNT(*) >= 10
      )
      AND
      customer_id IN (
        SELECT customer_id
        FROM public.rental
        GROUP BY customer_id
        HAVING COUNT(*) >= 10
      )
  );
