UPDATE public.film
SET rental_duration = 3, rental_rate = 9.99
WHERE title = 'Prison Break';

-- Update rental duration and rental rate for "Second War"
UPDATE public.film
SET rental_duration = 3, rental_rate = 9.99
WHERE title = 'Second War';


